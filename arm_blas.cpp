/// @file arm_blas.cpp
///


#include <assert.h>
#include "arm_compute/core/NEON/kernels/assembly/arm_gemm.hpp"
#include "arm_compute/runtime/CPUUtils.h"

#include "arm_blas.h"
#include <mutex>

/// Convert CBLAS enum for matrix transpose to arm_gemm convention.
static inline bool is_cblas_transpose(CBLAS_TRANSPOSE transpose) {
  return transpose == CblasNoTrans ? false : true;
}

/// @return CPU information.
/// @todo: it would be better to put the initialization code in a separate init routine
static const arm_compute::CPUInfo& get_ci() {
    static arm_compute::CPUInfo cpuInfo;
    static bool cpuInfoInitialized = false;
    if (!cpuInfoInitialized) {
        static std::mutex cpuInfoInitMutex;
        std::lock_guard<std::mutex> lck(cpuInfoInitMutex);
        if (!cpuInfoInitialized)  // Check again to be sure not to initialize twice
            arm_compute::get_cpu_configuration(cpuInfo);
        cpuInfoInitialized = true;
    }
    return cpuInfo;
}

void arm_cblas_init() {
  get_ci();
}

template<typename IN_TYPE, typename OUT_TYPE>
void cblas_gemm_armcl(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, const int M, const int N, const int K,
                 const OUT_TYPE alpha, const IN_TYPE *A, const int lda,
                 const IN_TYPE *B, const int ldb, const OUT_TYPE beta, OUT_TYPE *C,
                 const int ldc) {
  assert(layout == CblasRowMajor);

  bool trA = is_cblas_transpose(TransA);
  bool trB = is_cblas_transpose(TransB);
  const int max_threads = 1;

  auto gemm_operation = arm_gemm::gemm<IN_TYPE, OUT_TYPE>(get_ci(), M, N, K, 1, 1, trA, trB, alpha, beta, max_threads, false);
  size_t workingSize = gemm_operation->get_working_size();
  uint8_t* tmpMemory = new uint8_t[workingSize];
  gemm_operation->set_working_space(tmpMemory);
  gemm_operation->set_arrays(A, lda, 0, 0, B, ldb, 0, C, ldc, 0, 0);
  auto windowSize = gemm_operation->get_window_size();
  gemm_operation->execute(0, windowSize, 0);

  delete[] tmpMemory;
}

void cblas_sgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, const int M, const int N, const int K,
                 const float alpha, const float *A, const int lda,
                 const float *B, const int ldb, const float beta, float *C,
                 const int ldc) {
    cblas_gemm_armcl<float, float>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}

#ifdef __ARM_FP16_ARGS
void cblas_hgemm(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                 CBLAS_TRANSPOSE TransB, const int M, const int N, const int K,
                 const __fp16 alpha, const __fp16 *A, const int lda,
                 const __fp16 *B, const int ldb, const __fp16 beta, __fp16 *C,
                 const int ldc) {
    cblas_gemm_armcl<__fp16, __fp16>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}
#endif

void cblas_gemm_s16(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                    CBLAS_TRANSPOSE TransB, const int M, const int N,
                    const int K, const int32_t alpha, const int16_t *A,
                    const int lda, const int16_t *B, const int ldb,
                    const int32_t beta, int32_t *C, const int ldc) {
#ifdef __aarch64__
    cblas_gemm_armcl<int16_t, int32_t>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
#else
    printf("int16_t gemm is only supported for 64-bit architectures\n");
    assert(false);
#endif
}

void cblas_gemm_s8(CBLAS_ORDER layout, CBLAS_TRANSPOSE TransA,
                   CBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const int32_t alpha, const int8_t *A,
                   const int lda, const int8_t *B, const int ldb,
                   const int32_t beta, int32_t *C, const int ldc) {
#ifdef __aarch64__
    cblas_gemm_armcl<int8_t, int32_t>(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
#else
    printf("int8_t gemm is only supported for 64-bit architectures\n");
    assert(false);
#endif
}
